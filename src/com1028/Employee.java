package com1028;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Employee {
	
	private int employeeNumber;
	public static ArrayList<Employee> emp  = new ArrayList<>();
	
	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	
	public Employee() {
	}

	public static void req3() {
		BaseQuery order = new BaseQuery("root", "");
		  try {
	            ResultSet rs = order.useTable("employees");
	            while (rs.next()) {
	                Employee employee = new Employee();
	                employee.setEmployeeNumber(rs.getInt("employeeNumber"));
	                emp.add(employee);
	            }
	        } 
	        catch (SQLException e) {
	            System.out.println(e);
	        }   
	}

	@Override
	public String toString() {
		return "Employee [employeeNumber=" + employeeNumber + "]";
	}
}
