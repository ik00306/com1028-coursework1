package com1028;

import java.sql.*;
import java.util.*;

public class Payment {
	
	private int customerNumber;
	private String checkNumber;
	private int paymentDate;
	private double amount;
	public static ArrayList<Payment> pay  = new ArrayList<>();

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}
 
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public int getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(int paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Payment() {
	}

	public static void fillList() {
        BaseQuery order = new BaseQuery("root", "");
        try {
            ResultSet rs = order.useTable("payments");
            while (rs.next()) {
                Payment payment = new Payment();
                payment.setCustomerNumber(rs.getInt("customerNumber"));
                payment.setCheckNumber(rs.getString("checkNumber"));
                payment.setAmount(rs.getDouble("amount"));
                pay.add(payment);
            }

        } 
        catch (SQLException e) {
            System.out.println(e);
        }   
}

	@Override
	public String toString() {
		return "Payment [customerNumber=" + customerNumber + ", checkNumber=" + checkNumber + ", paymentDate="
				+ paymentDate + ", amount=" + amount + "]";
	}
}
