package com1028;

import java.sql.*;
import java.util.ArrayList;

public class Orders{

	private int orderNumber;
	private int orderDate;
	private double priceEach;
	private int customerNumber;
	
	public Orders() {
		   
	}
	
	public Orders(int orderNumber, int orderDate, double priceEach, int customerNumber) {
		super();
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.priceEach = priceEach;
		this.customerNumber = customerNumber;
	}
	
	public static void main(String[] args) {
		BaseQuery order = new BaseQuery("root", "");
		try {
			ResultSet rs = order.useTable("orderdetails");
			ArrayList<Integer> price = new ArrayList<>(); 
			while (rs.next()) {
				 int total = rs.getInt("priceEach") * rs.getInt("quantityOrdered");
				if(total >= 5000) {
					price.add(total);
				}
			 }
			System.out.println(price);		
		} catch (SQLException e) {
			e.printStackTrace();
		}
	} 
	
	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
	}


	}


