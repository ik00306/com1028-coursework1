package com1028;

import java.sql.*;
import java.util.*;

public class Customer {
	
	private int customerNumber;
	private int salesRepEmployeeNum;
	private String firstName;
	private String lastName;

	public int getSalesRepEmployeeNum() {
		return salesRepEmployeeNum;
	}

	public void setSalesRepEmployeeNum(int salesRepEmployeeNum) {
		this.salesRepEmployeeNum = salesRepEmployeeNum;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Customer() {
		
	}

	public int getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(int customerNumber) {
		this.customerNumber = customerNumber;
			}
	
		public static void main(String[] args) {
			ArrayList<Customer> price = new ArrayList<>();
			BaseQuery order = new BaseQuery("root", "");
			try {
				ResultSet rs = order.useTable("customers");
				while (rs.next()) {
					Customer customer = new Customer();
					customer.setCustomerNumber(rs.getInt("customerNumber"));
					customer.setFirstName(rs.getString("contactFirstName"));
					customer.setLastName(rs.getString("contactLastName"));
					customer.setSalesRepEmployeeNum(rs.getInt("salesRepEmployeeNumber"));
					price.add(customer);
				
					if(Payment.pay.isEmpty()) 			
						Payment.fillList();{
					}
						double total = 0.0;
						for(Payment payment : Payment.pay){
						    if(payment.getCustomerNumber() == customer.getCustomerNumber()){
						    		total += payment.getAmount();      
						    } 	
						} 	
						System.out.println("Customer: " + customer.getFirstName() + " " + customer.getLastName() + ", " + customer.getCustomerNumber() + " Amount spent: "  + total); 
					if(Employee.emp.isEmpty()) {
						Employee.req3();
					} 
					double revenue = 0;
					for(Employee employee : Employee.emp) {
						if(employee.getEmployeeNumber() == customer.getSalesRepEmployeeNum()) {
							revenue += total;
							System.out.println("Employee number: " + employee.getEmployeeNumber() + " Revenue: " + revenue);
						}
					}
				 }					
			} catch (SQLException e) {
}			
		}
		@Override
		public String toString() {
			return "Customer [customerNumber=" + customerNumber + ", salesRepEmployeeNum=" + salesRepEmployeeNum
					+ ", firstName=" + firstName + ", lastName=" + lastName + "]";
		}
}

			
