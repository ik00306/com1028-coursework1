	package com1028;

import java.sql.*;
import java.util.*;
import com.mysql.cj.conf.ConnectionUrl.Type;

public class BaseQuery {

	protected Connection mycon;
	private final String db = "jdbc:mysql://localhost:3306/classicmodels";
 
	public BaseQuery(String username, String password) {
		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
			mycon = DriverManager.getConnection(db, "root", "");
		}
		
		catch(Exception e) {
			System.out.println(e);
		}
	}
		protected ResultSet useTable(String tableName) throws SQLException{
			String query = "select * from " + tableName;
			Statement s = mycon.createStatement();
			ResultSet rs = s.executeQuery(query);
			return rs; 
			
		}
	
	}
