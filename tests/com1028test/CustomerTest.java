package com1028test;

import static org.junit.Assert.*;
import java.sql.*;
import java.util.ArrayList;

import org.junit.Test;

import com1028.Orders;
import com1028.BaseQuery;

import com1028.Customer;
public class CustomerTest {
	
	@Test
	public void testReq2() throws Exception {
		try {
			 Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/classicmodels", "root", "");
			 Customer customers = new Customer();
			 customers.setCustomerNumber(2);
			 Statement stmt = myConn.createStatement();
			 ArrayList<Customer> n = new ArrayList<>();
			 ResultSet rs = stmt.executeQuery("select * FROM payments INNER JOIN customers ON payments.customerNumber = customers.customerNumber");
			String m = null;	
			 while(rs.next()) {
					 m = rs.getString(1);
				}
				assertEquals(m,customers.getCustomerNumber());
		}
		 catch (Exception exc) {
			 exc.printStackTrace ();
		 }
	}

}
